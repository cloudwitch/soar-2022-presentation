#!/bin/bash

# For live development.
docker run --rm \
    -v "$(pwd)"/resources:/resources \
    -v "$(pwd)"/docs/slides:/reveal/docs/slides \
    -v "$(pwd)"/images:/reveal/images \
    -e TITLE='Migrating to Arm64 for Fun and Profit'\
    -e THEME_CSS='blood.css' \
    -p 8000:8000 \
    pheonix991/reveal.js:latest

# add port 35729 for dev.
