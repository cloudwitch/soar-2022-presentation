# Presentation

To view slide show, run the `run.sh` script.


```md
## Hotkeys

* `S`: show Speaker Notes
* `?`: show all Hotkeys
* `Esc`: show slide overview ("grid")
* Cursors: Navigation (on slides "grid")
* `Space`: next slide
* `,`: toggle presenter Mode (➡ becomes next slide - helpful for some presenters)
* `Ctrl` + `Shift` + `F`: Search slides
* `Ctrl` + `Alt` + Click: Zoom

Note:
* Speaker notes go here
```
