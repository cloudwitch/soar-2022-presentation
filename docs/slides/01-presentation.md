### Who is Fiona?

- Sr. Cloud DevOps engineer at Southwest.
- Platform OpsSuite NOC team.
- 1 year at Southwest.

Note:
- Who am I?
- I am a Sr. Cloud DevOps engineer at Southwest
- on the Platform OpsSuite NOC team.
- I've been here for about a year.



### AWS is offering Arm64 CPUs

- They call them Graviton.
- Arm64 architecture unlike x86_64 you see in AMD and Intel CPUs.

Note:
- AWS is making their own CPUs now.
- They call them Graviton, and they're already on their 3rd generation.
- Unlike AMD and Intel, the CPUs use the Arm64 CPU architecture.
- This is the same architecture used by Smart Phones, the Nintendo Switch, and the latest Apple Macs.



### Why move to Arm64?

- Graviton in the AWS cloud is cheaper than AMD or Intel instances.
- Graviton is also more powerful for certain workloads.

Note:
- Why move to Arm?
- Graviton in the AWS cloud is cheaper than AMD or Intel instances.
- Graviton is also more powerful for certain workloads.



### F1 and Arm64

- In 2021, Formula 1 reported an 80% reduction in Computational Fluid Dynamics simulation time while also reducing cost by 30% by switching to Graviton 2. <sup>[1](https://aws.amazon.com/solutions/case-studies/formula-1-graviton2/​)

Note:
- Back in 2021, Formula 1 reported an 80% reduction in Computational Fluid Dynamics simulation time while also reducing cost by 30% by switching to Graviton 2.
- The combination of saving money and not sacrificing any performance was enough to convince us.



### What issues do we need to overcome?

- x86_64 apps do not run on Arm64, so we have to build Arm versions of our software.
- Cannot migrate all apps at once.
- During the migration, apps must be able to run on x86_64 and Arm64.

Note:
- Why is moving to Arm hard?
- 1st, x86 apps do not run on Arm, so we have to build Arm versions of our software.
- 2nd, we have to eat the elephant one bite at a time, we can't migrate all our apps at once.
- 3rd, while we migrate, we need to be able to roll back to the x86 version at any time in the event any incompatibilities are found.



### What's the plan of attack?

##### Part 1

1. Learn how to use Docker Buildx to build new container images that run on Arm and x86.
1. Modify our Jenkins automation pipelines to build Arm64 and x86_64 versions of our apps.

Note:
- 1, learn how to use Docker Buildx to build new container images that run on Arm and x86.
- 2, modify our Jenkins automation pipelines to build Arm64 versions of our apps.



### What's the plan of attack?

##### Part 2

3. Deploy our Arm apps in Kubernetes clusters and make it look like nothing happened.
4. Profit.

Note:
- 3, Deploying the new versions to Kubernetes should be transparent to our users.
- And finally, relax and enjoy the profit.



### Docker Buildx

- Docker Buildx allows us to build Arm64 and x86_64 images on the same server in parallel.
- Push the image to the repository with a single tag.
    - No need to worry about building two versions of the app.

Note:
- Docker Buildx allows us to build Arm and x86 images on the same server in parallel.
- Push the image to the repository with a single tag.
- No need to worry about building two versions of the app.
- We have found that most of our apps _just work_ when built on Arm.



#### Jenkins automation

- We already use Jenkins to build our containers, so Buildx should be a simple update for our app teams in their automation.
- We were able to make this a one character code change via our Shared Library.

Note:
- Jenkins was already building the containers, so we wanted Buildx to be a simple update for our app teams in their automation.



### Jenkins automation 2

- Groovy method `buildAndPush` becomes `buildxAndPush`.

Build command before:

```bash
docker build -f ${dockerfile} --no-cache -t ${imageTag} .
```

Build command after:

```bash
docker buildx build --platform linux/amd64,linux/arm64 \
    -f ${dockerfile} --no-cache ${tagList} --push .
```

Note:
- A simple one character code change in via Jenkins Shared Library unlocks Arm functionality for our developers.
- The method build-And-Push became build-x-and-push.



### Pay the piper

- Building two images at a time.
    - Increases build times.
- Arm64 code runs slow on x86_64.
    - Increases build times.
- x86_64 code runs incredibly slow on Arm64.
    - Takes ~80-90 minutes using an Arm64 instance.

Note:
- At some point you have to pay the piper.
- Add the fact that non-native CPU code runs slower when emulated.



### Piper part 2

- Dockerfile optimization can be done to reduce build times.
    - Our Ubuntu base image builds are 8-12 minutes now.
- Building on containers on Intel instances is best.

Note:
- However, through some Dockerfile optimization, we were able to negate the these losses.
- We find container builds are about 85% quicker using Intel m6i instances than Graviton 2 instances.



### Kubernetes

- Deploying the Arm64 compatible app in Kubernetes is straight forward:
    1. Update container image tag.
    1. Add Arm64 node toleration.

Note:
- Deploying the Arm64 compatible app in Kubernetes is straight forward:
    - 1st, update the container image tag in the manifest.
    - 2nd, add Arm toleration to allow the container to run on Arm nodes.
    - 3rd, deploy.



### Profit

Enjoy the spoils.

| Environment | Cost on x86_64 | Cost on Arm64 | Daily Savings |
| --- | --- | --- | --- |
| QA | $181.74/day | $155.80/day | 14.27% |

Note:
- Now we get to enjoy the spoils of our effort.
- These figures only account for our Operations workload that run things like Grafana and Prometheus.
- Arm is saving us about 14% on top of using Spot instances in our QA environment.



### Production Profit

| Environment | Cost on x86_64 | Cost on Arm64 | Daily Savings |
| --- | --- | --- | --- |
| Production | $77.29​/day | $43.13/day​ | 44.2% |

Note:
- Production running On-Demand instances saves about 44%, about $12,000 a year.
- These factors only account for about 10% of our workload.
- I expect these number to be even more dramatic as the majority of our apps are migrated and instances are right-sized.



### Performance

- Graviton 2 performed 2.55% better than AMD in testing.
- Graviton 3 performed 32.48% better than AMD while still costing less.​

<font size="4">

| Instance Type | CPU Type | Run time | % faster than AMD | % faster than Graviton 2 | On-Demand Cost/Hr |
| --- | --- | --- | --- | --- | --- |
| c6a.2xlarge | AMD EPYC 7R13 | 9.72 seconds | | | $0.306 |
| c6g.2xlarge | AWS Graviton 2 | 9.54 seconds | 2.55% | | $0.272 |
| c7g.2xlarge | AWS Graviton 3 | 6.61 seconds | 32.48% | 30.71% | $0.29 |

</font>

Note:
- Using our CPU bound Line Handler application for performance testing, we saw the Graviton 2 instance performing 2.55% better than the equivalent AMD instance, and was 11% cheaper.
- Graviton 3, while still being cheaper than the AMD instance, was a massive 32% faster.



### AWS Lambda

- Lambda can now run on Graviton as well.
- AWS quotes up to 19% better performance at up to 20% lower cost. <sup>[1](https://aws.amazon.com/blogs/aws/aws-lambda-functions-powered-by-aws-graviton2-processor-run-your-functions-on-arm-and-get-up-to-34-better-price-performance/)</sup>
- Latest versions of these Runtimes are supported:​ <sup>[2](https://docs.aws.amazon.com/lambda/latest/dg/foundation-arch.html)</sup>
    - Python​
    - Node.js​
    - Java​
    - .NET Core​
    - Ruby​
    - Custom Runtime on Amazon Linux 2​

Note:
- AWS Lambda can run on Graviton as well.
- AWS quotes up to 19% better performance at up to 20% lower cost.
- Python an Node are simple as specifying arm64 runtime in Cloudformation or Serverless Framework.



### Where else can we use Graviton?

- Opensearch​
- RDS​
- Aurora​
- Fargate​
- ECS​
- Batch​
- And many more!​

Note:
- On top of EC2 and Lambda, my team has converted out Opensearch cluster to Graviton.
- AWS also offers Graviton on RDS, Aurora, ECS, and I'd run out of space on this slide if I included them.



### It's a whole new world

<img src="images/aladdin-jasmin-carpet_orig.jpg" height="583" width="1000"/>

Note:
- It's a whole new world of possibilities!!!
- I hope I've convinced you to try your hand at migrating to Arm.
- I promise it isn't as scary as it looks!
